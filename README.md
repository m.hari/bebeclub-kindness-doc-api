# Bebeclub Kindness Book API Documentation 
The 1st online book channel for kids, to keep learning and practicing kindness even nowadays.

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

This document contains the reference and guideline for Bebeclub Kindness Book API Services. This API allows you to integrate your system into Insignia API Service. You can manage token for key to unlock specific catalog using all API’s that we provided on this document. Bebeclub Kindness Book API Services is a JSON-based OAuth2 API. All requests must be secure (https) and authenticated.

| Host | Uri | Description |
| ------ | ------ | ------ | 
| staging | https://loyalapi-bebelac.stag-rewardx.insignia.co.id | testing environment |
| production | - | live  |

Note: We don't have any sandbox environment for test, so you can using a postman to test manually our api.

<a href="https://www.postman.com/"><img src="https://getlogovector.com/wp-content/uploads/2020/07/postman-inc-logo-vector.png" width="200" height="100"></a>

### Status Code

- **200** Success.
- **100** Data not found (member, token etc).
- **404** Bad request invalid parameters request
- **500** Internal server error


### Api Changelog
- [2021-01-01] - xxx

## Token Generation Module API

##### A. Token Generation.
Generate token for specific user

| Name | Type | Description |
| ------ | ------ | ------ | 
| member_id | int |  |
| transaction_date | datetime |  |
| items | array |  |
| value | string |  |
| qty | int | |

API specification
- Endpoint: **/Voucher/TokenGeneration**
- HttpMethod: **POST**
- Type: **JSON**

Request Body (example):
```sh
{
  "member_id": 99084,
  "transaction_date": "2021-01-01T12:00:00",
  "items": [
   {
     "value" : "800g",
     "qty" : 2
   }
  ]
}
```

Response Body (example):
```sh
{
    "StatusCode": "200",
    "StatusMessage": "Success",
    "Value": {
        "name": "jhon due",
        "email": "muhammad.hari@insignia.co.id",
        "token_code": "X143",
        "description": "Simpan kode token Anda, Mohon untuk tidak membagikan kode token Anda pada siapapun",
        "total_token": 4
    }
}
```

##### B. Token Redeem.
Redeem token for unlock specific access

| Name | Type | Description |
| ------ | ------ | ------ | 
| member_id | int |  |
| transaction_date | datetime |  |
| catalog_id | int |  |
| token_code | string |  |

API specification
- Endpoint: **Voucher/RedeemToken**
- HttpMethod: **POST**
- Type: **JSON**

Request Body (example):
```sh
{
  "member_id": 4,
  "transaction_date": "2021-01-01T12:00:00",
  "catalog_id": 1,
  "token_code": "XJXO"
}
```

Response Body (example):
```sh
{
    "StatusCode": "200",
    "StatusMessage": "Success",
    "Value": {
        "name": "jhon due",
        "email": "muhammad.hari@insignia.co.id",
        "token_code": "IA0J",
        "description": "valid token",
        "total_token": 2
    }
}
```

##### C. Get User Member Token.
Get token information

| Name | Type | Description |
| ------ | ------ | ------ | 
| member_id | int |  |

API specification
- Endpoint: **Voucher/GetMemberTokens?memberID={id}**
- HttpMethod: **GET**
- Type: **JSON**


Response Body (example):
```sh
{
    "StatusCode": "200",
    "StatusMessage": "Success",
    "Value": {
        "name": "Wati Purnamasari",
        "email": "suparmandadang1@gmail.com",
        "token_code": "4OG3",
        "description": "valid token",
        "total_token": 2,
        "child": [
            {
                "parent_id": 99084,
                "gender": "f",
                "name": "robert",
                "place_of_birth": "",
                "birthdate": "2018-06-01T00:00:00"
            }
        ]
    }
}
```


## License
MIT

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [dill]: <https://github.com/joemccann/dillinger>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [john gruber]: <http://daringfireball.net>
   [df1]: <http://daringfireball.net/projects/markdown/>
   [markdown-it]: <https://github.com/markdown-it/markdown-it>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [2021-01-01]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
   [PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>
   [PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>
